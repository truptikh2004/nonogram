.. Nonogram documentation master file, created by
   sphinx-quickstart on Sun Jun  4 18:37:27 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Nonogram's documentation!
====================================
This is the documentation of all the functions and their explanation written for the noonogram game.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Contents
--------

.. toctree::

   usage

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
