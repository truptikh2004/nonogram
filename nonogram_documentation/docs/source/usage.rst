Functions Written
=================

1. Clues
--------
* It takes the parameter size of the grid and list of inputs for rows or columns.

* User will input the valid clues for rows and columns.

2. Obvious_shade_row
--------------------
* It will shade all the obvious blocks to be shaded according to the clues with a '*' and will block those blocks which can't have anything in it with ' ' and remaining blocks will be '_'.

3. Transpose
------------

* Wrote this function so that the same logic for obvious_shade_column can be applied to columns as well.

4. Obvious_shade_column
-----------------------

* just return the grid formed by passing grid in Obvious_shade_row to transpose as grid to this function.

5. basic_grid
-------------

* Formation of the grid.

6. after_initial_solving
------------------------

* Works on the non obvious part of the problem. Works by replacing the '*' either with '_' or or with ' ' according to the conditions.

