def clues(size: int, row_or_col: str) -> list[list[int]]:
    clues =[[]]
    for i in range(size):
        lst = list(map(int, input(f"Enter the clues for the {row_or_col} {i+1} :").split(",")))
        s = sum(lst)
        while(s > size):
            s = 0
            lst = list(map(int,input(f"Enter valid clues for the {row_or_col} {i+1} :").split(",")))
            s = sum(lst)
        clues.append(lst)
    return clues[1:]

def obvious_shade_row(grid: list[list[str]], row_clues: list[list[int]]) -> list[list[str]]:
    for i in range(len(row_clues)):
        if row_clues[i] == [1, 1, 1]:
            grid[i][0], grid[i][1], grid[i][2], grid[i][3], grid[i][4] = 'x', ' ', 'x',  ' ', 'x'
        elif row_clues[i] == [3, 1]:
            grid[i][0], grid[i][1], grid[i][2], grid[i][3], grid[i][4] = 'x', 'x', 'x', ' ', 'x'
        elif row_clues[i] == [5]:
            grid[i][0], grid[i][1], grid[i][2], grid[i][3], grid[i][4] = 'x', 'x', 'x', 'x', 'x'
        elif row_clues[i] == [3]:
            grid[i][2] = 'x'
        elif row_clues[i] == [4]:
            grid[i][1], grid[i][2], grid[i][3] = 'x', 'x', 'x'
        elif row_clues[i] == [2, 2]:
            grid[i][0], grid[i][1], grid[i][2], grid[i][3], grid[i][4] = 'x', 'x', ' ', 'x', 'x'
        elif row_clues[i] == [1,3]:
            grid[i][0], grid[i][1], grid[i][2], grid[i][3], grid[i][4] = 'x',' ', 'x', 'x', 'x'
        elif row_clues[i] == [2,1]:
            grid[i][1] = 'x'
        elif row_clues[i] == [1,2]:
            grid[i][3] = 'x'
    return grid

def transpose(grid: list[list[str]]) -> list[list[str]]:
    return [[row[j] for row in grid] for j in range(len(grid))]

def obvious_shade_column(grid: list[list[str]], column_clues: list[list[int]]) -> list[list[str]]:
    return transpose(obvious_shade_row(transpose(grid), column_clues))

def basic_grid(size: int) -> list[list[str]]:
    return [['_' for i in range(size)] for j in range(size)]


def is_valid(grid: list[list[str]] ,transpose_grid: list[list[str]], row_clues: list[list[int]], column_clues: list[list[int]]) -> bool:
    return all([(sum(row_clues[pos]) == grid[pos].count('x')) for pos, row in enumerate(row_clues)]) and \
           all([(sum(column_clues[pos]) == transpose_grid[pos].count('x')) for pos, column in enumerate(column_clues)])

def after_initial_solving(grid_line : list[str], single_line_clue : list[int]) -> list[str]:
    clue2 = ["2", "11"]
    clue1 = ["1"]
    clue3 = ["3"]
    if ''.join(str(i) for i in single_line_clue) in clue2:
        if grid_line.count('*') == 2 :
            return list((''.join(grid_line)).replace('_', ' '))
        elif grid_line.count('*') < 2 :
            return list((''.join(grid_line)).replace('_', '*'))
    elif ''.join(str(i) for i in single_line_clue) in clue1:
        if grid_line.count('*') == 1:
            return list((''.join(grid_line)).replace('_', ' '))
        elif grid_line.count('*') < 1:
            return list((''.join(grid_line).replace('_', '*')))
    elif ''.join(str(i) for i in single_line_clue) in clue3:
        if grid_line.count('*') == 3:
            return list((''.join(grid_line)).replace('_', ' '))
        elif grid_line.count('*') < 3:
            return list((''.join(grid_line)).replace('_', '*'))
    return grid_line
